package permissions.db;

public interface RepositoryCatalog {
	
	public PersonRepository people();
	public AddressRepository addresses();
	public PermissionRepository permissions();
	public RoleRepository roles();
	public UserRepository users();
}
