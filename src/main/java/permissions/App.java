package permissions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import org.hsqldb.lib.HsqlArrayHeap;

import permissions.db.PagingInfo;
import permissions.db.RepositoryCatalog;
import permissions.db.catalogs.HsqlRepositoryCatalog;
import permissions.db.repos.HsqlAddressRepository;
import permissions.db.repos.HsqlPermissionRepository;
import permissions.db.repos.HsqlPersonRepository;
import permissions.db.repos.HsqlRoleRepository;
import permissions.db.repos.HsqlUserRepository;
import permissions.domain.Address;
import permissions.domain.Permission;
import permissions.domain.Person;
import permissions.domain.Role;
import permissions.domain.User;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

    	String url = "jdbc:hsqldb:hsql://localhost/workdb";
		try(Connection connection = DriverManager.getConnection(url)) 
		{
		          
	        HsqlPersonRepository repo = new HsqlPersonRepository(connection);
	    	Person person = new Person();
	    	person.setName("Bolesław");
	    	person.setSurname("Prus");
	    	repo.add(person);
	    	
            HsqlAddressRepository rep = new HsqlAddressRepository(connection);
	    	Address address = new Address();
	    	address.setCountry("Poland");
	    	address.setCity("Olsztyn");
	    	address.setStreet("Wojska");
	    	address.setPostalCode("10-200");
	    	address.setHouseNumber("14");
	    	address.setLocalNumber("3");
	    	rep.add(address);
	    	
	    	HsqlPermissionRepository reps = new HsqlPermissionRepository(connection);
		    Permission permission = new Permission();
		    permission.setName("Full");
		    reps.add(permission);
	    	
		    HsqlRoleRepository repi = new HsqlRoleRepository(connection);
		    Role role = new Role();
		    role.setName("Admin");
		    repi.add(role);
		    
		    HsqlUserRepository repu = new HsqlUserRepository(connection);
	    	User user = new User();
	    	user.setUsername("Piekarz");
	    	user.setPassword("chleb");
	    	repu.add(user);
	    	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
}
}