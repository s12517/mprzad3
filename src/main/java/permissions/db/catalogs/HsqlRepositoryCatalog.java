package permissions.db.catalogs;

import java.sql.Connection;

import permissions.db.AddressRepository;
import permissions.db.PermissionRepository;
import permissions.db.PersonRepository;
import permissions.db.RepositoryCatalog;
import permissions.db.RoleRepository;
import permissions.db.UserRepository;
import permissions.db.repos.HsqlAddressRepository;
import permissions.db.repos.HsqlPermissionRepository;
import permissions.db.repos.HsqlPersonRepository;
import permissions.db.repos.HsqlRoleRepository;
import permissions.db.repos.HsqlUserRepository;

public class HsqlRepositoryCatalog implements RepositoryCatalog{

	Connection connection;
	
	public HsqlRepositoryCatalog(Connection connection) {
		this.connection = connection;
	}

	public PersonRepository people() {
		return new HsqlPersonRepository(connection);
	}
	public AddressRepository addresses() {
		return new HsqlAddressRepository(connection);
	}
	
	public PermissionRepository permissions() {
		return new HsqlPermissionRepository(connection);
	}
	
	public RoleRepository roles() {
		return new HsqlRoleRepository(connection);
	}
	
	public UserRepository users() {
		return new HsqlUserRepository(connection);
	}
}
